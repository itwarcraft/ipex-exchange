package ai.turbochain.ipex.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author jack
 * @description 统计对象(用于接受统计数据)
 * @date 2020/1/8 16:16
 */
@Data
public class Statistics {
    Date date;
    Integer i;
}
