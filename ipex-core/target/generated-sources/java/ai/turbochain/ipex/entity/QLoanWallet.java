package ai.turbochain.ipex.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLoanWallet is a Querydsl query type for LoanWallet
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLoanWallet extends EntityPathBase<LoanWallet> {

    private static final long serialVersionUID = -935942683L;

    public static final QLoanWallet loanWallet = new QLoanWallet("loanWallet");

    public final NumberPath<java.math.BigDecimal> balance = createNumber("balance", java.math.BigDecimal.class);

    public final NumberPath<Long> coinId = createNumber("coinId", Long.class);

    public final NumberPath<java.math.BigDecimal> frozenBalance = createNumber("frozenBalance", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> isLock = createEnum("isLock", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final NumberPath<Long> memberId = createNumber("memberId", Long.class);

    public final StringPath unit = createString("unit");

    public final NumberPath<Integer> version = createNumber("version", Integer.class);

    public QLoanWallet(String variable) {
        super(LoanWallet.class, forVariable(variable));
    }

    public QLoanWallet(Path<? extends LoanWallet> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLoanWallet(PathMetadata metadata) {
        super(LoanWallet.class, metadata);
    }

}

